import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'

Vue.use(VueRouter)
const routes = [{
        path: '/',
        redirect: '/viewer'
    },
    {
        path: '/viewer',
        component: (resolve) => require(['../views/index'], resolve),
        meta: {
            title: '文档展示器'
        },
    },
    {
        path: '/ppt',
        component: (resolve) => require(['../components/pptx/pptx.vue'], resolve),
        meta: {
            title: 'ppt预览',
        }
    },
    {
        path: '/doc',
        component: (resolve) => require(['../components/word/Word.vue'], resolve),
        meta: {
            title: 'doc预览',
        }
    },
    {
        path: '/excel',
        component: (resolve) => require(['../components/excel/Excel.vue'], resolve),
        meta: {
            title: 'excel预览',
        }
    }, {
        path: '/pdf',
        component: (resolve) => require(['../components/pdf/Pdf.vue'], resolve),
        meta: {
            title: 'pdf预览',
        }
    }, {
        path: '/img',
        component: (resolve) => require(['../components/image/Image.vue'], resolve),
        meta: {
            title: '图片预览',
        }
    }, {
        path: '/mp4',
        component: (resolve) => require(['../components/mp4/index.vue'], resolve),
        meta: {
            title: '视频预览',
        }
    }, {
        path: '/code',
        component: (resolve) => require(['../components/codemirror/demo.vue'], resolve),
        meta: {
            title: '文本代码预览',
        }
    }, {
        path: '/music',
        component: (resolve) => require(['../components/music/index.vue'], resolve),
        meta: {
            title: '音乐预览',
        }
    }
]

const router = new VueRouter({
    mode: 'history',
    routes: routes
})
export default router